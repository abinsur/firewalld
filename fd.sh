#!/usr/bin/env bash
# filename: fd.sh
#
####################################
### abinsur<at>cryptolab<dot>net ###
####################################
#

#---user root
if [[ ${EUID} -ne 0 ]]; then
    echo -e $R"it as to be root"$DF
    echo 
    exit
fi

set_ffw(){
#---dependencies
if [[ ! -x $(command -v /usr/bin/firewalld) ]];then
    pacman -S --noconfirm firewalld
fi

#---enable firewalld
systemctl enable --now firewalld.service
sleep 1

#---configure firewall
firewall-cmd --permanent --new-zone=aux
systemctl restart firewalld.service
firewall-cmd --set-default-zone=aux
firewall-cmd --permanent --zone=aux --set-target=DROP
firewall-cmd --permanent --zone=aux --add-service={ssh,cockpit,dns,http,https,minidlna}
firewall-cmd --permanent --zone=aux --add-port={3333/tcp,8112/tcp,3000/tcp,3306/tcp}
firewall-cmd --permanent --zone=aux --add-icmp-block-inversion
firewall-cmd --permanent --zone=aux --add-icmp-block={echo-reply,echo-request}
firewall-cmd --permanent --zone=aux --add-rich-rule='rule family=ipv4 source address=10.3.3.0/24 accept'
firewall-cmd --set-log-denied=all
sleep 1
firewall-cmd --reload
firewall-cmd --info-zone=aux
}



#---menu
case "$1" in
    "-fwd") set_ffw;;
    *) echo "usage:$0 | -fwd |"
esac

















