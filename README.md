<div background="red" align="left">

[<img src="https://firewalld.org/images/firewalld.org.png">](https://firewalld.org/)

</div>

# [FIREWALLD](https://firewalld.org/)

## GENERAL
self explanatory
~~~
firewall-cmd --version
~~~
~~~
firewall-cmd --state
~~~
~~~
firewall-cmd --reload
~~~
~~~
firewall-cmd --complete-reload
~~~
~~~
firewall-cmd --zone=aux --add-forward
~~~
~~~
firewall-cmd --check-config
~~~

persistent mode
~~~
firewall-cmd --runtime-to-permanent
~~~


## LOGS
self explanatory
~~~
firewall-cmd --get-log-denied
~~~

options **all|unicast|broadcast|multicast|off**
~~~
firewall-cmd --set-log-denied=[OPTIONS]
~~~

view denied packets
~~~
dmesg | grep -i REJECT
~~~
or
~~~
dmesg | grep -i DROP
~~~
or
~~~
journalctl -x -e
~~~


## ZONES
active zone list
~~~
firewall-cmd --get-default-zone
~~~

list all zones
~~~
firewall-cmd --get-zones
~~~

list zone details
~~~
firewall-cmd --list-all-zones
~~~

add new zone
~~~
firewall-cmd --permanent --new-zone=aux
~~~

self explanatory
~~~
firewall-cmd --permanent --delete-zone=aux
~~~

set the default zone
~~~
firewall-cmd --set-default-zone=aux
~~~


self explanatory
~~~
firewall-cmd --permanent --path-zone
~~~
~~~
firewall-cmd --permanent --zone=aux --set-description=zone description
~~~
~~~
firewall-cmd --permanent --zone=aux --get-description
~~~
~~~
firewall-cmd --permanent --zone=aux --set-short=short description
~~~
~~~
firewall-cmd --permanent --zone=aux --get-short
~~~


list active zone and interfaces and which zone they are in
~~~
firewall-cmd --permanent --get-active-zone
~~~

create preconfigured zone from an existing zone
~~~
firewall-cmd --new-zone-from-file=[NAME-ZONE] --name=aux
~~~

active zone info
~~~
firewall-cmd --info-zone=aux
~~~
~~~
firewall-cmd --list-all --zone=aux
~~~

## ASSOCIATE

inteface zone
~~~
firewall-cmd --get-zone-of-interface=enp2s0
~~~

adds interface for zone
~~~
ip a
~~~
~~~
firewall-cmd --permanent --zone=aux --add-interface=enp2s0
~~~

associate interface to another zone
~~~
firewall-cmd --zone=public --change-interface=enp1s1
~~~

self explanatory
~~~
firewall-cmd --zone=aux --list-interfaces
~~~

## TARGET
self explanatory
~~~
firewall-cmd --permanent --zone=aux --get-target
~~~

target **accept|drop|reject**
~~~
firewall-cmd --permanent --zone=aux --set-target=[TARGET]
~~~

## RICH RULES

- *rule [family="(rule family)"]*
-  *[ source address="(address)" [invert="True"] ]*
-  *[ destination address="(address)" [invert="True"] ]*
-  *[ (element) ]*
-  *[ log [prefix="(prefix text)"] [level="(log level)"] [limit value="(rate/duration)"] ]*
-  *[ audit [limit value="(rate/duration)"] ]*
-  *[ (action) ]*
----------------------------------------
- *family= ipv4 or ipv6*
- *source address= IP or subnet , source mac= mac-address*
- *destination address= IP or subnet*
- *service name=“(service name)”*
- *port port=“(port value)” protocol=“tcp|udp”*
- *Log or Audit*
- *Actions : accept | reject [type=“(reject type)”] | drop*
----------------------------------------

options **accept|drop|reject**
~~~
firewall-cmd --zone=aux --add-rich-rule='rule family=ipv4 source address=10.3.3.144 port port=1900 protocol=udp accept'
~~~
~~~
firewall-cmd --zone=aux --permanent --add-rich-rule='rule family=ipv4 source address=10.3.3.144/24 accept'
~~~

## SERVICES/PORTS/SOURCES
list all available services
~~~
firewall-cmd --get-services
~~~

list of services in the zone
~~~
firewall-cmd --zone=aux --list-services
~~~

add service
~~~
firewall-cmd --permanent --zone=aux --add-service=ssh
~~~

list ports in the zone
~~~
firewall-cmd --zone=aux --list-ports
~~~

add ports
~~~
firewall-cmd --permanent --zone=aux --add-port=1900/udp
~~~

list of allowed ips
~~~
firewall-cmd --zone=aux --list-sources
~~~

allowed ips
~~~
firewall-cmd --permanent --zone=aux --add-source=10.3.3.133
~~~

allow ip range
~~~
firewall-cmd --permanent --zone=aux --add-source=10.3.3.0/24
~~~

self explanatory
~~~
firewall-cmd --zone=aux --list-all
~~~
~~~
firewall-cmd --list-all
~~~

## POLICIES
self explanatory
~~~
firewall-cmd --get-policies
~~~
~~~
firewall-cmd --list-all-policies
~~~

## ICMP
self explanatory
~~~
firewall-cmd --get-icmptypes
~~~
~~~
firewall-cmd --info-icmptype=echo-request
~~~
~~~
firewall-cmd --query-icmp-block=echo-request
~~~
~~~
firewall-cmd  --permanent --add-icmp-block=echo-request
~~~

## PANIC
self explanatory
~~~
firewall-cmd --panic-on
~~~
~~~
firewall-cmd --panic-off
~~~
~~~
firewall-cmd --query-panic
~~~